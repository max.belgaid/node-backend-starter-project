import { sayHello } from './index';

describe('starter project', () => {
  it('should say hello to newcomers', () => {
    expect(sayHello()).toEqual('Hello from starter project');
  });
});
